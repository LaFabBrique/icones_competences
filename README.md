# Icônes compétences La Fab'Brique

Création d'icônes pour le [tableau de références](https://lafabbrique.org/wiki/lafabbrique:tableau_de_references) des membres du FabLab.

![](icones_competences_0.1.png)

Ressources graphiques / Inspirations: Librairie [OpenClipart](https://openclipart.org) et/ou créations originales.
